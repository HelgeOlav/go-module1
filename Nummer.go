// This module serves only the purpose of trying to set up A new module package to be imported into another program
package go_module1

// This struct contains two numbers that we can do operations upon.
// The members are called A and B.
type Nummer struct {
	A int
	B int
}
