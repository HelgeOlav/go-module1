package go_module1

// Gjennomsnitt returns the average of the two numbers in the struct Nummer.
func (n Nummer) Gjennomsnitt() int {
	return (n.A + n.B) / 2
}