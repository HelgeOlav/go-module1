package go_module1

// Summer works on the struct Nummer and returns the sum of both members of the struct
func (n Nummer) Summer() int {
	return n.A + n.B
}