package go_module1

import "testing"

func nummerFactory() Nummer {
	return Nummer{A: 3, B: 5}

}

func TestNummer_Summer(t *testing.T) {
	svar := nummerFactory().Summer()
	if svar == 8 {
		return
	}
	t.Fatal("Summer failed")
}

func TestNummer_Gjennomsnitt(t *testing.T) {
	svar := nummerFactory().Gjennomsnitt()
	if svar == 4 {
		return
	}
	t.Fatalf("Gjennomsnitt failed, got %d", svar)
}